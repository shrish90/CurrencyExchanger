package com.exchanger.Service.ServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.exchanger.DAO.OrderDAO;
import com.exchanger.Model.Request;
import com.exchanger.Service.OrderService;

@Service
public class OrderServiceImpl implements OrderService{
	@Autowired
	OrderDAO dao;
	public Request createOrder(Request order) {
		dao.createOrder(order);
		return order;
	}
	
}
