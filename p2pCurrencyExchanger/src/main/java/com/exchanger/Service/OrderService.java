package com.exchanger.Service;


import com.exchanger.Model.Request;

public interface OrderService {
	Request createOrder(Request order);
}
