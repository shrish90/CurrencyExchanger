package com.exchanger.DAO.DAOImpl;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.exchanger.DAO.OrderDAO;
import com.exchanger.Model.Request;

@Component
public class OrderDAOImpl implements OrderDAO{

    @Autowired
    JdbcTemplate jdbcTemplate;
	
   

	public int createOrder(Request orderDto) {
		 
		    
		jdbcTemplate.update("INSERT INTO currency_exchange(user_id,from_Country,to_country,order_amount,Order_Time) values (?,?,?,?,?)",
				orderDto.getUserId(),
				orderDto.getFrom(),
				orderDto.getTo(),
				orderDto.getToSell(),
				new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())
				);
		
		
		return 0;
	}


	
}
