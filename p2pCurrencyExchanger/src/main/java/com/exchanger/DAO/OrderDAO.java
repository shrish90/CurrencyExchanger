package com.exchanger.DAO;

import com.exchanger.Model.Request;

public interface OrderDAO {

	int createOrder(Request orderDto);

}
