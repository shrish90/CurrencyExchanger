package com.exchanger.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.exchanger.Model.Request;
import com.exchanger.Service.OrderService;

@RestController
@RequestMapping(value = "/exchange")
public class ExchangeController {
	@Autowired
	OrderService OrderService;
	@RequestMapping(value = "/exchangeCurrency", 
			method = RequestMethod.POST,
			consumes="application/json",
			produces="application/json")
	public ResponseEntity<Request> test(@RequestBody String apiRequestObj) {
		Request requestObj = new Request();
	    ObjectMapper mapper = new ObjectMapper();
	    try {
			requestObj = mapper.readValue(apiRequestObj, Request.class);
			OrderService.createOrder(requestObj);
			return new ResponseEntity<Request>(requestObj, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Request>(requestObj, HttpStatus.FORBIDDEN);
			
		}
	    
	}
	@RequestMapping("/test")
	public @ResponseBody void test() {
		System.out.println("came");
	}
}
