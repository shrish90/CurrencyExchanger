create database currency_exchange;

use currency_exchange;


create table Exchange_Order (
Order_Id int NOT NULL AUTO_INCREMENT,
User_Id int,
From_Country varchar(20),
To_country varchar(20),
Order_Amount DOUBLE ,
Order_Time DATETIME,
PRIMARY KEY (Order_Id)

);

create table Currency_Exchange (
Id int NOT NULL ,
From_Country varchar(20),
To_country varchar(20),
Exchange_Rate float,
Exchange_fees float,
PRIMARY KEY (FROM_COUNTRY, To_Country)
);

create table users (
userid int ,
name varchar(50),
country varchar(50)
);

create table Processed_Orders(
order_id int,
Status varchar(50),
Paid_amount Double,
Order_Processed_time date,
FOREIGN KEY (Order_Id) REFERENCES Exchange_Order(Order_Id));